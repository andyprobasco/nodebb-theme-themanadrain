BUGS:
problems with people who have switched skins
"full screen button when writing a post may be broken"
"some threads may not be in the right place"
"on mobile, links to news articles are out of place

FEATURE REQUESTS/COMPLAINTS:
dark skin

too many links/cards at top of screen

ME:
change font size in posts


530 billion


things I could want -

early retirement (MAYBE)??
travel
toys
nicer location


money can turn into -
travel
stuff
rent


##
- work remotely/trello
    - travel cheap
    - money -> tickets to friends
    - money ... earlier retirement to work on passion projects ... maybe travelling will inspire writing/working more?

- work at a game company (denver)
    - money -> tickets, time travel
    - reduces need for personal projects?
    - local friends for gaming probably?

- cleveland
    - money -> tickets/travel
    - I'll want to travel more because the weather sucks


read cracking code interview
read pragmatic programmer
do codewars puzzle
work on TMD
work on roguelike
go for a walk to get tea
game


CSS fixes

"continue reading" button on mobile
"unread" page entirely
fancy first post, header underlines
decklist CSS
// test alternate header 
... dark mode.
