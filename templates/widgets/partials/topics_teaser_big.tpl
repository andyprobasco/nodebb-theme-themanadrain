<!-- BEGIN topics -->
<li class="clearfix widget-topic-big">


	<a href="{relative_path}/topic/{topics.slug}">
	<!-- IF topics.thumb -->
		<img src="{topics.thumb}" class="lv-img widget-topic-big-thumbnail" title="{topics.title}" />
	<!-- ELSE -->
		<!-- IF topics.user.picture -->
			<img title="{topics.user.username}" class="avatar avatar-sm not-responsive widget-topic-big-thumbnail" src="{topics.user.picture}" />
		<!-- ELSE -->
			<div class="avatar avatar-sm not-responsive widget-topic-big-thumbnail" style="background-color: {topics.user.icon:bgColor};">{topics.user.icon:text}</div>
		<!-- ENDIF topics.user.picture -->
	<!-- ENDIF topics.thumb -->
	</a>


	<a href="{relative_path}/topic/{topics.slug}">
		<article>
			<h1>{topics.title}</h1>
			<p>
				{topics.teaser.content}
			</p>
		</article>
	</a>

	<footer>
		<span>By </span><a href="<!-- IF topics.user.userslug -->{relative_path}/user/{topics.user.userslug}<!-- ELSE -->#<!-- ENDIF topics.user.userslug -->">{topics.user.username}</a>
		<span class="timeago" title="{topics.timestampISO}"></span>
		<span class="postcount">{topics.postcount}</span>
	</footer>
</li>
<!-- END topics -->
