<div class="widget-recent-topics-big">
    <ul data-numtopics="{numTopics}">
    <!-- IMPORT widgets/partials/topics_teaser_big.tpl -->
    </ul>
    <!-- IF cid -->
    <a href="category/{cid}"><button component="topic/reply" class="btn btn-md btn-primary waves-effect">More</button></a>
    <!-- ENDIF cid -->
</div>

<script>
'use strict';
/* globals app, socket, translator, templates, utils*/
(function() {
    function onLoad() {
        var	topics = $('#recent_topics_big');
        app.createUserTooltips();
        processHtml(topics);
        var recentTopicsWidget = app.widgets.recentTopics;
        var numTopics = parseInt(topics.attr('data-numtopics'), 10) || 8;
        if (!recentTopicsWidget) {
            recentTopicsWidget = {};
            recentTopicsWidget.onNewTopic = function(topic) {
                var recentTopics = $('#recent_topics_big');
                if (!recentTopics.length) {
                    return;
                }
                app.parseAndTranslate('partials/topics', { topics: [topic] }, function(html) {
                    processHtml(html);
                    html.hide()
                        .prependTo(recentTopics)
                        .fadeIn();
                    app.createUserTooltips();
                    if (recentTopics.children().length > numTopics) {
                        recentTopics.children().last().remove();
                    }
                });
            };
            app.widgets.recentTopics = recentTopicsWidget;
            socket.on('event:new_topic', app.widgets.recentTopics.onNewTopic);
        }
        function processHtml(html) {
            html.find('span.timeago').timeago();
        }
    }
    if (window.jQuery) {
        onLoad();
    } else {
        window.addEventListener('load', onLoad);
    }
})();
</script>
