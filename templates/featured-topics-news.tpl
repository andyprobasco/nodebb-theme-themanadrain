<div class="fte-theme-tmd">
  <!-- BEGIN topics -->
  <div class="topic section sectionMain recentNews" tid="{topics.tid}">
    <div class="posts">
      <div class="primaryContent leftDate">

        <div class="thumbnail">
          <!-- IF topics.thumb -->
          <img src="{topics.thumb}" align="left" itemprop="image" />
          <!-- ELSE -->
            <a href="<!-- IF topics.user.userslug -->/user/{topics.user.userslug}<!-- ELSE -->#<!-- ENDIF topics.user.userslug -->">
            <!-- IF topics.user.picture -->
            <img component="user/picture" data-uid="{topics.user.uid}" src="{topics.user.picture}" align="left" itemprop="image" />
            <!-- ELSE -->
            <div component="user/picture" data-uid="{topics.user.uid}" class="user-icon" style="background-color: {topics.user.icon:bgColor};">{topics.user.icon:text}</div>
            <!-- ENDIF topics.user.picture -->
            <i component="user/status" class="fa fa-circle status {topics.user.status}" title="[[global:{topics.user.status}]]"></i>
            </a>
          <!-- ENDIF topics.thumb -->
        </div>


        <div class="messageContent">
          <div class="subHeading">
            <a href="/topic/{topics.slug}" class="newsTitle">{topics.title}</a>
            <span class="comments">
              <a href="/topic/{topics.slug}"><i class="fa fa-comment"> {topics.replies} </i></a>
            </span>
          </div>

          <div class="content newsText">{topics.post.content}</div>
          <div class="clearFix"></div>
        </div>

        <div class="sectionFooter">

          <div class="postedBy">
            <strong>
              <a href="<!-- IF topics.user.userslug -->/user/{topics.user.userslug}<!-- ELSE -->#<!-- ENDIF topics.user.userslug -->" itemprop="author" data-username="{topics.user.username}" data-uid="{topics.user.uid}">{topics.user.username}</a>
            </strong>
          </div>
          <div class="postInfo">
            <span class="date">{topics.date.start}</span>
            <span class="views">({topics.viewcount} Views / {topics.post.votes} Upvotes)</span>
          </div>

          <a class="btn btn-primary" href="/topic/{topics.slug}">
            <i class="fa fa-reply fa-rotate-180"></i> Continue reading...
          </a>
        </div>

      </div>
    </div>
  </div>

  <!-- IF !@last -->
  <div widget-area="contentbetween"><!-- BEGIN widgets.contentbetween -->{widgets.contentbetween.html}<!-- END widgets.contentbetween --></div>
  <!-- ENDIF !@last -->

  <!-- END topics -->

  <div style="position:relative;">
  <!-- IF paginator -->
  <div class="section sectionMain">
    <div class="PageNav">
      <nav>
        <!-- IF prevpage -->
        <a href="{featuredRoute}{prevpage}" class="btn btn-default">&lt; Previous Page</a>
        <!-- ENDIF prevpage -->

        <!-- BEGIN pages -->
        <a href="{featuredRoute}{pages.number}" class="btn <!-- IF pages.currentPage -->btn-primary<!-- ELSE -->btn-default<!-- ENDIF pages.currentPage -->">{pages.number}</a>
        <!-- END pages -->

        <!-- IF nextpage -->
        <a href="{featuredRoute}{nextpage}" class="btn btn-default">Next Page &gt;</a>
        <!-- ENDIF nextpage -->
      </nav>
    </div>
  </div>
  <!-- ENDIF paginator -->
  <a href="/category/3/vintage-news" style="position:absolute; right: 0; top: 0;" class="btn btn-primary pull-right">All News</a>
  </div>
</div>
