<!-- added mobile progress bar to bottom of file -->
<div class="topic">
    <!-- IMPORT partials/breadcrumbs.tpl -->
    <div class="card">
        <div class="listview lv-lg">
            <div class="lv-header-alt" component="post/header">
                <div class="title">
                    <i class="fa fa-thumb-tack <!-- IF !pinned -->hidden<!-- ENDIF !pinned -->"></i> <i class="fa fa-lock <!-- IF !locked -->hidden<!-- ENDIF !locked -->"></i> <span class="topic-title" component="topic/title">{title}</span>
                    <ul class="lv-actions actions">
                        <!-- IMPORT partials/topic/sort.tpl -->
                    </ul>
                </div>
            </div>

            <!-- IF merger -->
            <div component="topic/merged/message" class="alert alert-warning clearfix">
                <span class="pull-left">[[topic:merged_message, {mergeIntoTid}, {merger.mergedIntoTitle}]]</span>
                <span class="pull-right">
                    <a href="{config.relative_path}/user/{merger.userslug}">
                        <strong>{merger.username}</strong>
                    </a>
                    <small class="timeago" title="{mergedTimestampISO}"></small>
                </span>
            </div>
            <!-- ENDIF merger -->

            <div component="topic/deleted/message" class="lv-header-alt <!-- IF !deleted --> hidden<!-- ENDIF !deleted --> clearfix">
                <span class="pull-left">[[topic:deleted_message]]</span>
                <span class="pull-right">
                    <!-- IF deleter -->
                    <a href="{config.relative_path}/user/{deleter.userslug}">
                        <strong>{deleter.username}</strong>
                    </a>
                    <small class="timeago" title="{deletedTimestampISO}"></small>
                    <!-- ENDIF deleter -->
                </span>
            </div>

            <div class="lv-body">
                <div component="topic" data-tid="{tid}" data-cid="{cid}">
                <!-- BEGIN posts -->
                    <!-- IMPORT partials/topic/post.tpl -->
                    <!-- IF !posts.index -->
                    <div class="post-bar-placeholder"></div>
                    <!-- ENDIF !posts.index -->
                <!-- END posts -->
                </div>

                <div class="post-bar">
                    <!-- IMPORT partials/post_bar.tpl -->
                </div>
            </div>
        </div>
    </div>
    <!-- IF config.usePagination -->
        <!-- IMPORT partials/paginator.tpl -->
    <!-- ENDIF config.usePagination -->
    <div class="pagination-block-spacer"></div>
    <div class="visible-xs visible-sm pagination-block text-center">
        <div class="progress-bar"></div>
        <div class="wrapper">
            <i class="fa fa-2x fa-angle-double-up pointer fa-fw pagetop"></i>
            <i class="fa fa-2x fa-angle-up pointer fa-fw pageup"></i>
            <span class="pagination-text"></span>
            <i class="fa fa-2x fa-angle-down pointer fa-fw pagedown"></i>
            <i class="fa fa-2x fa-angle-double-down pointer fa-fw pagebottom"></i>
        </div>
    </div>

</div>
<!-- IF !config.usePagination -->
    <noscript>
    <!-- IMPORT partials/paginator.tpl -->
    </noscript>
<!-- ENDIF !config.usePagination -->
