<!-- IMPORT partials/breadcrumbs.tpl -->
<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>

<div class="card">
	<div class="listview lv-bordered lv-lg">
		<div class="lv-header-alt" component="unread/header">
			<div class="markread pull-right<!-- IF !topics.length --> hidden<!-- ENDIF !topics.length -->">
				<button type="button" id="markSelectedRead" class="btn btn-default">Clear Selected</button>
				<button type="button" id="markAllRead" class="btn btn-default">Clear All</button>
			</div>
		</div>
		<div class="lv-body">
			<div id="category-no-topics" style="margin-top:15px;" class="alert alert-warning <!-- IF topics.length -->hidden<!-- ENDIF topics.length -->">[[unread:no_unread_topics]]</div>

			<a href="{config.relative_path}/{selectedFilter.url}{querystring}">
				<div class="alert alert-warning hide" id="new-topics-alert"></div>
			</a>

			<!-- IMPORT partials/topics_list.tpl -->
			<button id="load-more-btn" class="btn btn-primary hide">[[unread:load_more]]</button>
			<!-- IF config.usePagination -->
				<!-- IMPORT partials/paginator.tpl -->
			<!-- ENDIF config.usePagination -->
		</div>
	</div>
</div>
