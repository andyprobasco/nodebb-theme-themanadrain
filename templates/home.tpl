<!-- IMPORT partials/breadcrumbs.tpl -->
<div widget-area="header">
    <!-- BEGIN widgets.header -->
    {{widgets.header.html}}
    <!-- END widgets.header -->
</div>
<div class="row">
    <div class="category <!-- IF widgets.sidebar.length -->col-lg-9 col-sm-12<!-- ELSE -->col-lg-12<!-- ENDIF widgets.sidebar.length -->">
        <!-- IMPORT partials/category/subcategory.tpl -->
        <div class="clearfix">
            <a href="{url}" class="inline-block">
                <div class="alert alert-warning hide" id="new-topics-alert"></div>
            </a>
        </div>

        <hr class="hidden-xs" />

        <p class="hidden-xs">{name}</p>

        <!-- IMPORT partials/topics_list.tpl -->

    </div>
    <div widget-area="sidebar" class="col-lg-3 col-sm-12 <!-- IF !widgets.sidebar.length -->hidden<!-- ENDIF !widgets.sidebar.length -->">
        <!-- BEGIN widgets.sidebar -->
        {{widgets.sidebar.html}}
        <!-- END widgets.sidebar -->
    </div>
</div>
<div widget-area="footer">
    <!-- BEGIN widgets.footer -->
    {{widgets.footer.html}}
    <!-- END widgets.footer -->
</div>
