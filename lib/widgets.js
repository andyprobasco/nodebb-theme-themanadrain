
const fs = require('fs');
const path = require('path');

const async = require.main.require('async');
const nconf = require.main.require('nconf');
const topics = require.main.require('./src/topics');

let app;



"use strict";

var categories = require.main.require('./src/categories');


function initWidgets (params, callback) {
    app = params.app;
    callback();
}


function defineWidgets (widgets, callback) {
    widgets.push({
        widget: 'categorycards',
        name: 'Category Cards',
        description: 'Lists forum categories in material-style cards',
        content: fs.readFileSync(path.resolve(__dirname, '../templates/admin/widgets/categorycardswidget.tpl')),
    });
    callback(null, widgets);
}


function renderCategoryCards(widget, callback) {

    let categoryData;
    let tree;

    async.waterfall([
        function (callback) {
            categories.getCategoriesByPrivilege('categories:cid', widget.uid, 'find', callback);
        },
        function (_categoryData, callback) {
            categoryData = _categoryData;
            tree = categories.getTree(categoryData, 0);
            categories.getRecentTopicReplies(categoryData, widget.uid, callback);
        },
        function () {
            tree.forEach(function (category) {
                if (category && Array.isArray(category.posts) && category.posts.length && category.posts[0]) {
                    category.teaser = {
                        url: nconf.get('relative_path') + '/post/' + category.posts[0].pid,
                        timestampISO: category.posts[0].timestampISO,
                        pid: category.posts[0].pid,
                        topic: category.posts[0].topic,
                    };
                }
            });
            app.render('widgets/categorycards', {
                categories: tree,
                relative_path: nconf.get('relative_path')
            }, function (err, html) {
                widget.html = html;
                callback(err, widget);
            });
        },
    ], callback);
};

function defineWidgetAreas(areas, callback) {
    areas = areas.concat([
        {
            name: "Category Content Top",
            template: "category.tpl",
            location: "contenttop"
        }
    ]);

    callback(null, areas);
}

module.exports = { defineWidgets, renderCategoryCards, initWidgets, defineWidgetAreas };
