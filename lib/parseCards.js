const decklistRegex = /<pre><code>([\s\S]*?)<\/code><\/pre>/mg
const cardNameRegex = /\[\[(.*?)\]\]/g
const decklistSubheadingRegex = /^([^\s\d].*?)$/mg
const decklistCardNameRegex = /^(\d+) (.*?)$/mg

function parseCardsInPost (page, callback) {
    page.postData.content = parseDecklist(page.postData.content)
    page.postData.content = parseCardNames(page.postData.content)
    callback(null, page);
}

function parseCardsRaw (raw, callback) {
    raw = parseDecklist(raw)
    raw = parseCardNames(raw)
    callback(null, raw)
}

function parseCardNames (raw) {
    let cardName;
    while ((cardName =  cardNameRegex.exec(raw)) !== null) {
        var rawCard = cardName[0]
        var name = cardName[1]
        raw = raw.replace(
            rawCard,
            '<a class="tmd-cardname" href="http://magiccards.info/query?q=!' + name.replace(' ', '+') + '">' + name + '</a>');
    }
    return raw
}

function parseDecklist (raw) {
    let decklists;

    let headingOpen = false;
    let bodyOpen = false;

    while ((decklists = decklistRegex.exec(raw)) !== null) {
        let rawDecklist = decklists[0];
        let decklistContent = decklists[1];
        if (decklistContent) {
            var parsedDecklist = '<div class="tmd-decklist">';
            parsedDecklist += decklistContent
                .replace(decklistSubheadingRegex, function (match, subheading) {
                    let htmlString = '';
                    if (subheading.lastIndexOf('# ', 0) === 0) {
                        if (bodyOpen === true) {
                            bodyOpen = false;
                            htmlString += '</div>'
                        }
                        if (headingOpen === false) {
                            headingOpen = true;
                            htmlString += '<div class="tmd-decklist-heading">';
                        }
                        return htmlString + '<h1>' + subheading.replace('# ', '') + '</h1>';
                    } else if (subheading.lastIndexOf('## ', 0) === 0) {
                        if (bodyOpen === true) {
                            bodyOpen = false;
                            htmlString += '</div>'
                        }
                        if (headingOpen === false) {
                            headingOpen = true;
                            htmlString += '<div class="tmd-decklist-heading">';
                        }
                        return htmlString + '<h2>' + subheading.replace('## ', '') + '</h2>';
                    } else {
                        if (headingOpen === true) {
                            headingOpen = false;
                            htmlString += '</div>'
                        }
                        if (bodyOpen === false) {
                            bodyOpen = true;
                            htmlString += '<div class="tmd-decklist-body">';
                        }

                        return htmlString + '<h3>' + subheading + '</h3>';
                    }
                }).replace(decklistCardNameRegex, function (match, quantity, cardName) {
                    let htmlString = '';
                    if (headingOpen === true) {
                        headingOpen = false;
                        htmlString += '</div>'
                    }
                    if (bodyOpen === false) {
                        bodyOpen = true;
                        htmlString += '<div class="tmd-decklist-body">';
                    }
                    return htmlString + '<div>' + quantity + ' [[' + cardName.replace('<br />', '') + ']] </div>'
                });

            if (headingOpen === true) {
                headingOpen = false;
                parsedDecklist += '</div>'
            }

            if (bodyOpen === true) {
                bodyOpen = false;
                parsedDecklist += '</div>';
            }

            parsedDecklist += '</div>';
            raw = raw.replace(rawDecklist, parsedDecklist);
        }
    }
    return raw
}

module.exports = { parseCardsInPost, parseCardsRaw }
